from collections import Counter
from bitarray import bitarray

FirstCodes = []

class Node:
    def __init__(self, val = None, lt = None, rt = None):
        self.l = lt
        self.r = rt
        self.v = val

def MakeNewCodes(arr):
	NewC, t = {arr[0][0] : (bitarray('0') * arr[0][1])}, 0
	for i in range (1,len(arr)):
		t = (t + 1) * (2 ** (arr[i][1] - arr[i-1][1]))
		NewC[arr[i][0]] = bitarray('0') * (arr[i][1] - len(bin(int(t))[2:])) + bin(int(t))[2:]
	return NewC

# Arch block

def codes(cd, el):
	if el.l != None:
		codes(cd + '0', el.l)
	if el.r != None:
		codes(cd + '1', el.r)
	if el.l == None and el.r == None:
		FirstCodes.append((el.v, len(cd)))

def Hcomp(inf):
	arr = Counter(inf)
	nodes = [[Node(i), arr[i]] for i in arr]
	while len(nodes) > 1:
		nodes.sort(key=lambda x: x[1])
		nodes.append((Node(None, nodes[0][0], nodes[1][0]), ((nodes.pop(0))[1] + (nodes.pop(0))[1])))	
	
	codes('', nodes[0][0])
	FirstCodes.sort(key=lambda x: x[::-1])
	NewCodes = MakeNewCodes(FirstCodes)
	result = bytearray()
	for i in range(256):	
		try: result.append(len(NewCodes[chr(i)]))
		except: result.append(0)
	bt = bitarray()
	bt.encode(NewCodes, inf)
	result += bt.tobytes()
	return (result)

# Dearch block

def appendtree(SymCode, curNode, vl):
	if SymCode == '':
		curNode.v = vl
	else:
		if SymCode[0] == '0':
			if curNode.l == None:
				curNode.l = Node()
			appendtree(SymCode[1:], curNode.l, vl)
		if SymCode[0] == '1':
			if curNode.r == None:
				curNode.r = Node()
			appendtree(SymCode[1:], curNode.r, vl) 

def HDearch(inf):
	Alph, root, result = [], Node(), bitarray()
	for i in range(256):
		cur = inf[i]
		if ord(cur) != 0:
			Alph.append((chr(i), ord(cur)))	
	Alph.sort(key=lambda x: x[::-1])
	NewCodes = MakeNewCodes(Alph)
	result.frombytes(inf[256:])
	return(''.join(result.decode(NewCodes)))
		
