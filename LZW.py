import math
from bitarray import bitarray


def LZWcomp(inf):
	nextEl = inf[0]
	def outputCode(num):
		cursize = int(math.log(len(alphabet), 2)) + 1
		out.extend('0' * (cursize - len(bin(num)[2:])) + bin(num)[2:])


	def nextSymb(current, index):
		if alphabet.get(bytes(current)) == None:
			alphabet.setdefault(current, len(alphabet) + 1)		
			outputCode(alphabet[current[:-1]])
			nextEl = current[-1]
		else:
			nextEl = bytes(current) + bytes(inbytes[index])
			index+=1
		return (nextEl, index)
	index = 1
	alphabet = {chr(i): i for i in range(256)}
	inbytes, out = bytes(inf), bitarray()

	while index < len(inbytes):
		nextEl, index = nextSymb(nextEl, index)


	if alphabet.get(bytes(nextEl)) != None:
		outputCode(alphabet[bytes(nextEl)])
	elif nextEl != '':
		outputCode(alphabet[nextEl[:-1]])
		outputCode(alphabet[nextEl[-1]])

	return out.tobytes()

def LZWdecomp(inf):
	def bitstoint(st):
		res = 0
		for i in range(len(st)):
			if st[i] == '1':
				res += 2 ** (len(st) - i - 1)
		return res
	def ReadCode(index):
		curSize = int(math.log(len(alphabet) + 2, 2)) + 1
		r = inbytes[index:index + curSize]
		return bitstoint(r.to01())
		
	def nextCode(index, prevCode):
		code = ReadCode(index)
		index += int(math.log(len(alphabet) + 2, 2)) + 1
		if alphabet.get(code) == None:
			alphabet.setdefault(len(alphabet) + 1, alphabet[prevCode] + alphabet[prevCode][0])
			out.frombytes(alphabet[len(alphabet)])
		else:
			alphabet.setdefault(len(alphabet) + 1, alphabet[prevCode] + alphabet[code][0])
			out.frombytes(alphabet[code])
		return(index, code)

	index = 9
	alphabet = {i: bytes(chr(i)) for i in range(256)}
	inbytes, out = bitarray(), bitarray()
	inbytes.frombytes(inf)
	nextEl = ReadCode(0)
	out.frombytes(alphabet[nextEl])

	while index <= len(inbytes) + 1:
		index, nextEl = nextCode(index, nextEl)

	return out.tobytes()


# print(out)
# res = out
# print('*******')
# alphabet = {i: bytes(chr(i)) for i in range(256)}
# inbytes, out = res, bitarray()
# index = 0
# fc = ReadCode(0)
# print fc
# out.frombytes(alphabet[fc])
# nextCode(9, fc)
# print 'out: ', out
# c = bitarray()
# c.fromstring('aaaaaaaaaaa')
# print 'orig:', c
# print out.tostring()
