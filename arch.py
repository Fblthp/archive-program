import Huff, LZW, sys

def Nope(Info):
	return (Info)

def _extract(ar):
	f = open(ar[1], 'rb')
	if f.read(3) != 'UPA':
		print('invalid format')
		exit()
	Method= f.read(4)
	while True:
		Name = f.read(20).strip()
		FileSize = int('0' + f.read(20).strip())
		if FileSize == 0: exit()
		Info = f.read(FileSize)
		open(Name, 'wb').write(Dearchive_Methods[Method](Info))

def _add(ar):
	Method = open(ar[1], 'rb').read(7)[3:]

	fi = open(ar[1], 'ab')
	fi.write('{:<20}'.format(ar[2])) 

	Info = open(ar[2], 'rb').read()
	Compressed = Compression_Methods[Method](Info) 

	fi.write('{:<20}'.format(str(len(Compressed))))
	fi.write(Compressed)


def _create(ar):
	open(ar[2], 'wb').write('UPA' + '{:_<4}'.format(ar[1].lower()))
	_add(ar[1:])
	
def _help(ar):
	print(open('help.txt', 'r').read())	

Keys = {'-e':_extract, '-a':_add, '-c': _create, '-h': _help}
Compression_Methods = {'none': Nope, 'huff': Huff.Hcomp, 'lzw_': LZW.LZWcomp}
Dearchive_Methods = {'none': Nope, 'huff': Huff.HDearch, 'lzw_': LZW.LZWdecomp}


# try:
Keys[sys.argv[1]](sys.argv[1:])
# except:
	# print("Invalid syntax. Wrire '-h' for more details")

